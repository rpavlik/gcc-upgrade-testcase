#!/bin/sh
# SPDX-FileCopyrightText: 2020, Ryan Pavlik
# SPDX-License-Identifier: CC0-1.0
#
set -e
(
    cd $(dirname $0)
    docker build . -t upgradetest
)
exec docker run -it --rm upgradetest:latest
