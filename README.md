# Buster to Bullseye upgrade gcc-10 bug repro

See <https://bugs.debian.org/972820> for the bug in question.

This repro case uses Docker, but you could do the same stuff in a chroot.

To use:

- Run `./build-and-run.sh`: this will create a fairly minimal Debian Buster
  (slim) Docker image named `upgradetest`, with a sample set of minimal packages
  required to reproduce the bug. It will then launch it (in an auto-removing
  container) in interactive mode, so you'll be in a shell inside this Buster
  environment.
- Now you have choices:
  - Run `doupgrade.sh` in the container (it was copied to `/usr/local/bin` in
    the image) to swap out your sources and try upgrading to Bullseye - you'll
    see it doesn't work.
  - Rename `/etc/apt/sources.list.d/workaround.list.save` to
    `/etc/apt/sources.list.d/workaround.list` before you try upgrading, to add
    an OBS repo containing the workaround packages from
    <https://salsa.debian.org/rpavlik/gcc-10-compat>. This simulates a
    possible fix (adding transitional packages for bullseye/sid back to gcc-10 at least until the bullseye release), and will let you successfully upgrade to bullseye.
