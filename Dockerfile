# SPDX-FileCopyrightText: 2020, Ryan Pavlik
# SPDX-License-Identifier: CC0-1.0

# Reproduces an upgrade failure

FROM debian:buster-slim

# Copy an inactive sources.list file to use for a Bullseye upgrade
COPY sources.list.bullseye /etc/apt/

# Copy a shell script to start the Bullseye upgrade
COPY doupgrade.sh /usr/local/bin/

# Copy an inactive sources.list file to add Ryan's workaround transition packages.
COPY workaround.list.save /etc/apt/sources.list.d/
COPY workaround.asc /etc/apt/trusted.gpg.d/

RUN apt-get update && \
    apt-get install -y -qq --no-install-recommends gcc-8 libc6-dev libreoffice && \
    apt-get clean
