#!/bin/sh
# SPDX-FileCopyrightText: 2020, Ryan Pavlik
# SPDX-License-Identifier: CC0-1.0
set -e
cp /etc/apt/sources.list.bullseye /etc/apt/sources.list && \
  apt-get update && \
  apt-get dist-upgrade